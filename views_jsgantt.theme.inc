<?php

/**
 * @file
 */

/**
 * Implementation of template_preprocess
 */
function template_preprocess_views_view_views_jsgantt(&$vars) {
  $is_live_preview = !(stristr($_SERVER['REDIRECT_URL'], '/admin/build/views/ajax/preview/') === FALSE);
  if ($is_live_preview) {
    drupal_set_message(t('Live preview is not suported for jsgantt'));
    return;
  }
}

/**
 * Overridable theme function.
 * This functions prints a warning when no data is available.
 */
function theme_views_jsgantt_nodata() {
  drupal_set_message(t('There is no data available'), 'warning');
}
