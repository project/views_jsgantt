<?php

/**
 * @file
 */

$jsgantt_dir = drupal_get_path('module', 'views_jsgantt');
drupal_add_js($jsgantt_dir . "/jsgantt/jsgantt.js");
drupal_add_css($jsgantt_dir . "/jsgantt/jsgantt.css");
drupal_add_css($jsgantt_dir . "/views_jsgantt.css");
?>
<div style="position:relative" class="gantt" id="GanttChartDIV"></div>
<script language="javascript">
var g = new JSGantt.GanttChart('g',document.getElementById('GanttChartDIV'), '<?php print($options['format']); ?>');
g.setShowRes(0); g.setShowDur(0); g.setShowComp(0); g.setCaptionType('None'); g.setShowStartDate(0); g.setShowEndDate(0);
<?php
$last_group = '';
$startfield = $options['startdate']->field_alias;
$endfield = $options['enddate']->field_alias;
$parentidfield = $options['parentid']->field_alias;
$parenttitlefield = $options['parenttitle']->field_alias;
$colorby = $options['colorby']->field_alias;
$colorlist = $options['colorlist'];
$list = explode("\n", $colorlist);
$list = array_map('trim', $list);
$list = array_filter($list, 'strlen');
foreach ($list as $opt) {
  // Sanitize the user input with a permissive filter.
  $opt = content_filter_xss($opt);
  if (strpos($opt, '|') !== FALSE) {
    list($key, $value) = explode('|', $opt);
    $colors[$key] = (isset($value) && $value !=='') ? $value : $key;
  }
  else {
    $colors[$opt] = $opt;
  }
}
foreach ($view->result as $row) {
  $task = array();
  $task['parentid'] = $row->$parentidfield;
  $task['parenttitle'] = $row->$parenttitlefield;
  $task['nid'] = $row->nid;
  $task['title'] = $row->node_title;
  $task['start'] = date("n/j/Y", strtotime($row->$startfield));
  $task['end'] = date("n/j/Y", strtotime($row->$endfield));
  $task['link'] = url('node/' . $task['nid']);
  $task['color'] = $colors[$row->$colorby];
  if ($task['parentid'] != $last_group) {
    print("g.AddTaskItem(new JSGantt.TaskItem(" . $task['parentid'] . ",'" . $task['parenttitle'] . "','','','ff0000','" . $task['link'] . "',0,'',0,1,0,1));\n");
    $last_group = $task['parentid'];
  }
  print("g.AddTaskItem(new JSGantt.TaskItem(" . $task['nid'] . ",'" . $task['title'] . "','" . $task['start'] . "','" . $task['end'] . "','" . $task['color'] . "','" . $task['link'] . "',0,'',0,0," . $task['parentid'] . ",1));\n");
}
?>
g.Draw();
g.DrawDependencies();
</script>