<?php

/**
 * @file
 */

class views_jsgantt_style_plugin extends views_plugin_style {
  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['startdate'] = array('default' => '');
    $options['enddate'] = array('default' => '');
    $options['parentid'] = array('default' => '');
    $options['parenttitle'] = array('default' => '');
    $options['colorby'] = array('default' => '');
    $options['colorlist'] = array('default' => '');
    $options['format'] = array('default' => '');
    return $options;
  }

  /**
   * Provide form
   */
  function options_form(&$form, &$form_state) {
    $field_options = array();
    $fields = $this->display->handler->get_handlers('field');
    foreach ($fields as $id => $handler) {
      $field_options[$id] = $handler->ui_name(FALSE);
    }
    $form['startdate'] = array(
      '#title' => t('Start date field'),
      '#description' => t('Pick the date field that defines start dates.'),
      '#type' => 'select',
      '#options' => $field_options,
      '#default_value' => $this->options['startdate'],
    );
    $form['enddate'] = array(
      '#title' => t('End date field'),
      '#description' => t('Pick the field that defines end dates.'),
      '#type' => 'select',
      '#options' => $field_options,
      '#default_value' => $this->options['enddate'],
    );
    $form['parentid'] = array(
      '#title' => t('Parent ID field'),
      '#description' => t('Pick the field that defines the nid of parents of tasks.'),
      '#type' => 'select',
      '#options' => $field_options,
      '#default_value' => $this->options['parentid'],
    );
    $form['parenttitle'] = array(
      '#title' => t('Parent title field'),
      '#description' => t('Pick the field that defines the title of parents of tasks.'),
      '#type' => 'select',
      '#options' => $field_options,
      '#default_value' => $this->options['parenttitle'],
    );
    $form['colorby'] = array(
      '#title' => t('Color by field'),
      '#description' => t('Pick the field that defines the colors of tasks.'),
      '#type' => 'select',
      '#options' => $field_options,
      '#default_value' => $this->options['colorby'],
    );
    $form['colorlist'] = array(
      '#title' => t('Color list'),
      '#description' => t('An array of field values and corresponding HTML colors. Enter one value per line, in the format key|color.'),
      '#type' => 'textarea',
      '#default_value' => $this->options['colorlist'],
    );
    $form['format'] = array(
      '#title' => t('Default display format'),
      '#description' => t('Pick default display format.'),
      '#type' => 'select',
      '#options' => array('day' => 'Day', 'week' => 'Week', 'month' => 'Month', 'quarter' => 'Quarter'),
      '#default_value' => $this->options['format'],
    );
  }

  function render() {
    $this->options['startdate'] = $this->view->display_handler->get_handler('field', $this->options['startdate']);
    $this->options['enddate'] = $this->view->display_handler->get_handler('field', $this->options['enddate']);
    $this->options['parentid'] = $this->view->display_handler->get_handler('field', $this->options['parentid']);
    $this->options['parenttitle'] = $this->view->display_handler->get_handler('field', $this->options['parenttitle']);
    $this->options['colorby'] = $this->view->display_handler->get_handler('field', $this->options['colorby']);
    return theme($this->theme_functions(), $this->view, $this->options);
  }
}